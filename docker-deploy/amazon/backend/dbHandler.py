from ast import comprehension
import datetime
from pickle import FALSE
import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base

import proto.world_amazon_pb2 as pb2


'''
Handler has a Session field, which is session configure, call session object by Session()
e.g. h = Handler(); seesion = h.Session()
'''


class Handler:
    def __init__(self):
        psw = 'passw0rd'
        db = 'mydb'
        # create an engine
        # engine = create_engine('mysql+mysqlconnector://root:password@localhost:3306/test')
        pengine = sa.create_engine(
            'postgresql+psycopg2://postgres:' + psw + '@vcm-24373.vm.duke.edu/' + db)
        # define declarative base
        Base = declarative_base()
        # reflect current database engine to metadata
        metadata = sa.MetaData(pengine)
        metadata.reflect()
        # build your User class on existing `users` table

        class User(Base):
            __table__ = sa.Table("web_user", metadata)

        class Production(Base):
            __table__ = sa.Table("web_production", metadata)

        class Order(Base):
            __table__ = sa.Table("web_order", metadata)

        class OrderItem(Base):
            __table__ = sa.Table("web_orderitem", metadata)

        class Cart(Base):
            __table__ = sa.Table("web_cart", metadata)

        class Warehouse(Base):
            __table__ = sa.Table("web_warehouse", metadata)

        class Request(Base):
            __table__ = sa.Table("web_request", metadata)
        # call the session maker factory
        sessionFactory = sa.orm.sessionmaker(pengine)
        Session = sa.orm.scoped_session(sessionFactory)

        # assgin value to the Session generator
        self.Session = Session
        self.User = User
        self.Production = Production
        self.Order = Order
        self.OrderItem = OrderItem
        self.Cart = Cart
        self.Warehouse = Warehouse
        self.Request = Request

    def query(self):
        session = self.Session()
        # 创建Query查询，filter是where条件，最后调用one()返回唯一行，如果调用all()则返回所有行:
        user = session.query(self.User).filter(self.User.id == '1').one()
        # 打印类型和对象的name属性:
        print('type:', type(user))
        print('name:', user.name)
        # 关闭Session:
        self.Session.remove()

    def insert(self):
        session = self.Session()
        #new_user = self.User(id='5', name='Bob',password="111")
        new_request = self.Request(
            request="try this ", ack=False, sending_time=datetime.datetime.utcnow())
        # 添加到session:
        # session.add(new_user)
        session.add(new_request)
        # flush it
        session.flush()
        seq_num = new_request.seq_num
        # 提交即保存到数据库:
        session.commit()
        self.Session.remove()
        return seq_num

    '''
    @return a list of pairs: all the things and amounts in the order
    [(proId, amount)]
    '''

    def showOrderDetails(self, orderId):
        things = []  # list of pair (proId, amount)
        session = self.Session()
        # check the detailed products in orederitem table
        new_order = session.query(self.OrderItem).filter(
            self.OrderItem.order_id == orderId).all()
        for thing in new_order:
            print("To buy production id " + str(thing.production_id) +
                  " amount " + str(thing.amount))
            things.append((thing.production_id, thing.amount))
        self.Session.remove()
        return things

    def updateWhId(self, orderId, whId):
        session = self.Session()
        session.query(self.OrderItem).filter(
            self.OrderItem.order_id == orderId).update({"warehouse_id": whId})
        session.commit()
        self.Session.remove()

    def getPdDescrip(self, proId):
        session = self.Session()
        print("_________debug_____________________")
        print("PROD WE ARE LOOKING IS _____" + str(proId))
        prod = session.query(self.Production).filter(
            self.Production.id == proId).first()
        self.Session.remove()
        return prod.description

    '''
    1. add a fake command and get its id
    2. change the reqeust to real reqeust 
    Note: these two actions should be atomic, but hard to implement using sqlalchemy;
    fortunately this may not be a big problem in this project
    @return directly return the string format of the msg object
    
    Type:
    1 : purchaseMorecmd
    2 : packCmd
    3 : loadCmd
    
    
    '''
    
    def addReqToTb(self, cmdToSend, type):
        session = self.Session()
        reqFake = self.Request(request="try this".encode(), ack=False,
                               sending_time=datetime.datetime.utcnow())
        session.add(reqFake)
        session.flush()        # flush it
        seq_num = reqFake.seq_num
        session.commit()
        # now update the real request
        if (type == 1):
            cmdToSend.buy[0].seqnum = seq_num
        elif (type ==2 ):
            cmdToSend.topack[0].seqnum = seq_num
        elif (type ==3):
            cmdToSend.load[0].seqnum = seq_num
        
        reqRealStr = cmdToSend.SerializeToString()
        session.query(self.Request).filter(self.Request.seq_num ==
                                           seq_num).update({"request": reqRealStr})
        session.commit()
        self.Session.remove()
        return reqRealStr

    '''
    set the ack conlumn of request to true if recv ack back
    @param acks: a list of acks(int)   
    ack = seqnum  
    '''

    def ackReqSent(self, acks):
        for ak in acks:
            session = self.Session()
            session.query(self.Request).filter(
                self.Request.seq_num == ak).update({"ack": True})
            session.commit()
            self.Session.remove()

    '''
    return the list of missing msgStr that need to resend again
    '''

    def returnMissReq(self, longestWaiTime):
        msgStrs = []
        session = self.Session()
        reqNoAck = session.query(self.Request).filter(
            self.Request.ack == False).all()
        self.Session.remove()
        for req in reqNoAck:
            # compute the timedelt between sending time and current time
            current = datetime.datetime.utcnow()
            sendTime = req.sending_time
            timeDiff = current - sendTime
            if(timeDiff.total_seconds() > longestWaiTime):
                # msgStrs.append(req.request.encode())
                msgStrs.append(req.request)
        # self.Session.remove()
        return msgStrs


    def getOrderIdOfPurchaseMoreRsp(self, purchaseMore):
        session = self.Session()
        whId = purchaseMore.whnum
        # first get potential orders that status is 'buy' in that warehouse
        orders = session.query(self.Order, self.OrderItem).filter(self.Order.id == self.OrderItem.order_id).filter(
            self.OrderItem.warehouse_id == whId).filter(self.Order.status == "B").all()
        hashtable = dict()
        for p in orders:
            key = p[0].id
            value = p[1]
            hashtable.setdefault(key, []).append(value)
        orderIdLookingFor = None
        for orderId in hashtable.keys():
            findOrder = True
            for thing in purchaseMore.things:
                findThing = False
                for orderItem in hashtable.get(orderId):
                    if(orderItem.production_id == thing.id and orderItem.amount == thing.count):
                        findThing = True
                        break
                if findThing:
                    # try next orderItem
                    continue
                else:
                    # this order is not we are looking at
                    findOrder = False
                    break
            if findOrder:
                orderIdLookingFor = orderId
                break
        if orderIdLookingFor != None:
            # update that order status
            session.query(self.Order).filter(
            self.Order.id == orderIdLookingFor).update({"status": 'P'})
            session.commit()
        self.Session.remove()
        if orderIdLookingFor == None:
             raise LookupError("There is no corresponding order for this response in the database")
         
        return orderIdLookingFor
    
    
    def getTruckStatusForOrder(self, shipid):
        session = self.Session()
        order = session.query(self.Order).filter(
            self.Order.id == shipid).first()
        self.Session.remove()
        return order.truck_status
    
    def getTruckIdForOrder(self, shipid):
        session = self.Session()
        order = session.query(self.Order).filter(
            self.Order.id == shipid).first()
        self.Session.remove()
        return order.truck_id
    
    def getWhIdForOrder(self, shipid):
        session = self.Session()
        orderitem = session.query(self.OrderItem).filter(
            self.OrderItem.order_id == shipid).first()
        self.Session.remove()
        return orderitem.warehouse_id
    
    def updateStatusToLoad(self,shipid):
        session = self.Session()
        session.query(self.Order).filter(
        self.Order.id == shipid).update({"status": 'L'})
        session.commit()
        self.Session.remove()
        
        
    def updateStatusToTrans(self,shipid):
        session = self.Session()
        session.query(self.Order).filter(
        self.Order.id == shipid).update({"status": 'T'})
        session.commit()
        self.Session.remove()
        
        
    def updateStatusToDelivered(self,shipid):
        session = self.Session()
        session.query(self.Order).filter(
        self.Order.id == shipid).update({"status": 'D'})
        session.commit()
        self.Session.remove()
    
    
    # whInit is a list of three-tuple
    def setUpWh(self, whInit):
        session = self.Session()
        session.query(self.Warehouse).delete()
        for wh in whInit:
            new_wh = self.Warehouse( id = wh[0], x = wh[1], y = wh[2])
            session.add(new_wh)
        session.commit()
        self.Session.remove()
        
        
    def getDestXY(self,orderid):
        session = self.Session()
        order = session.query(self.Order).filter(
            self.Order.id == orderid).first()
        self.Session.remove()
        return order.destX, order.destY
    
    def getUpsAccount(self,orderid):
        session = self.Session()
        order = session.query(self.Order).filter(
            self.Order.id == orderid).first()
        self.Session.remove()
        return order.upsAcc
    
    
    def updateTruckId(self,orderId,truckId):
        session = self.Session()
        session.query(self.Order).filter(
        self.Order.id == orderId).update({"truck_id":truckId })
        session.commit()
        self.Session.remove()
    
    def updateTrackingId(self,orderId,trackingId):
        session = self.Session()
        session.query(self.Order).filter(
        self.Order.id == orderId).update({"tracking_number":trackingId })
        session.commit()
        self.Session.remove()
    
    def validUpsAcc(self,orderId):
        session = self.Session()
        session.query(self.Order).filter(
        self.Order.id == orderId).update({"upsAcc_valid":True})
        session.commit()
        self.Session.remove()
        
    def updateTruckStatus(self, truckId, wh_x, wh_y):
        session = self.Session()
        res = session.query(self.Order, self.OrderItem, self.Warehouse).filter(
            self.Order.id == self.OrderItem.order_id).filter(
            self.OrderItem.warehouse_id == self.Warehouse.id).filter(
            self.Warehouse.x == wh_x, self.Warehouse.y == wh_y).filter(
            self.Order.truck_id == truckId).all()
        print(res)
        idSet = set()
        for order in res:
            idSet.add(order[0].id)
        for id in idSet:    
            print(id)
            session.query(self.Order).filter(
            self.Order.id == id).update({"truck_status":True})
        session.commit()
        self.Session.remove()
        return truckId, wh_x, wh_y , res,idSet
        
    def resetOrderStatus(self,orderId):
        session = self.Session()
        session.query(self.Order).filter(
        self.Order.id == orderId).update({"status": 'B'})
        session.commit()
        self.Session.remove()
        
        
    def reOrder(self,orderId):
        # copy the order and add a new tuple
        session = self.Session()
        orderOld  = session.query(self.Order).filter(self.Order.id == orderId).first()
        orderObj = self.Order(
            destX =orderOld.destX, 
            destY = orderOld.destY, 
            status = 'B', 
            upsAcc =orderOld.upsAcc,
            user_id = orderOld.user_id,
            tracking_number = orderOld.tracking_number,
            truck_id = orderOld.truck_id,
            truck_status = False,
            upsAcc_valid = orderOld.upsAcc_valid)
        session.add(orderObj)
        # flush it
        session.flush()
        new_id = orderObj.id
        session.commit()
        # add corresponding items of order in orderItem table 
        items = session.query(self.OrderItem).filter(self.OrderItem.order_id == orderId).all()
        for item in items:
            print(item)
            newItem = self.OrderItem(amount = item.amount, order_id = new_id,production_id = item.production_id,
                                     warehouse_id = item.warehouse_id)

            session.add(newItem)
            print(newItem)
        session.commit()
        self.Session.remove()
        return new_id

if __name__ == "__main__":
    # main function here
    handler = Handler()
    # handler.query()
    # print(handler.insert())
    print(handler.showOrderDetails(6))
    # handler.updateWhId(6,1)
    print(handler.getPdDescrip(3))

    # aCmd = pb2.ACommands()
    # purchaseMore = aCmd.buy.add()
    # purchaseMore.whnum = 1
    # thing = purchaseMore.things.add()
    # thing.id = 1
    # thing.description = "apple"
    # thing.count = 99
    # purchaseMore.seqnum = 0

    # handler.addReqToTb(aCmd)
    # handler.ackReqSent([7])

    # pc = pb2.APurchaseMore()
    # pc.whnum = 1
    # thing = pc.things.add()
    # thing.id = 1
    # thing.description = "test"
    # thing.count = 1
    # thing = pc.things.add()
    # thing.id = 4
    # thing.description = "test"
    # thing.count = 1
    # thing = pc.things.add()
    # thing.id = 3
    # thing.description = "test"
    # thing.count =1
    # thing = pc.things.add()
    # thing.id = 2
    # thing.description = "test"
    # thing.count =1
    # print(handler.getOrderIdOfPurchaseMoreRsp(pc))
    
    # print(handler.updateStatusToTrans(24))
    # handler.updateTruckId(22,3)
    # handler.updateTrackingId(22,888)
    # handler.updateTruckStatus(1, 2, 3)
    print(handler.reOrder(121))
    