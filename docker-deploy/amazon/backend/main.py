from ntpath import join
import queue
from re import X
import threading
import time

import proto.world_amazon_pb2 as pb2
import proto.ups_pb2 as upb2

from google.protobuf.internal.decoder import _DecodeVarint32
from google.protobuf.internal.encoder import _EncodeVarint
import socket
import sys

import dbHandler
'''
There are two ways to send commands: bunch of reqeusts in one commands or one request in one commands

1)bunch of reqeusts in one commands: fewer sending times, less threads needed(or no threads needed), using a queue and
setting a fixed(or dynamic sending period)

2)one request in one commands: common multiplethreading

In our program, we use the second way
'''


'''
msg:message obejct
sock:socket that connected to the world server
'''


def sendMsg(msg, sock):
    string_message = msg.SerializeToString()
    _EncodeVarint(sock.send, len(string_message), None)
    sock.sendall(string_message)


def sendMsgStr(msgStr,sock):
    _EncodeVarint(sock.send, len(msgStr), None)
    sock.sendall(msgStr)

'''
receive messege from world server
Note: the return type is string, need to parse to specific type
'''


def recMsg(sock):
    var_int_buff = []
    while True:
        buf = sock.recv(1)
        var_int_buff += buf
        msg_len, new_pos = _DecodeVarint32(var_int_buff, 0)
        if new_pos != 0:
            break
    msgStr = sock.recv(msg_len)
    return msgStr


'''
Before connect to world socket then connect to one world, 
by default we use world 1, warehouse id: 1, x:1, y:2
if error return then throw valueerror exception
RETURN the socket that connected to the that world
'''


def connectToWorld(worldId=1, warehouses={(1, 1, 2)}):
    # ip_port = ('vcm-25303.vm.duke.edu', 23456)  # amazon port number is 12345
    ip_port = ('vcm-25953.vm.duke.edu', 23456)
    # ip_port = ('localhost',12345)
    s = socket.socket()     #
    s.connect(ip_port)      #
    print("sock connected")

    # init connect request
    connect = pb2.AConnect()
    connect.worldid = worldId
    for wh in warehouses:
        warehouse = connect.initwh.add()
        warehouse.id = wh[0]
        warehouse.x = wh[1]
        warehouse.y = wh[2]
    connect.isAmazon = 1

    # send connection request to world server
    sendMsg(connect, s)
    print("send connect to world request")
    # receive the response message from server
    respStr = recMsg(s)
    print("receive response")
    # since the response is type "connected", parse it
    acconected = pb2.AConnected()
    acconected.ParseFromString(respStr)
    print("the world connected is " + str(acconected.worldid))
    if (acconected.result != "connected!"):
        raise ValueError(acconected.result)

    else:
        print("connected!")
        return s

# class that will be passed into functions


class Production:
    def __init__(self, id, desc, count):
        self.id = id
        self.description = desc
        self.count = count
        pass

    def __str__(self):
        return "(" + str(self.id) + ", " + str(self.description) + str(self.count) + ")"

'''
return a list of production objects
'''
def generateProcutionsObjFromOrderId(orderId,handler):
    things = handler.showOrderDetails(orderId) #things:[(proId,amonut)]
    products = list(map(lambda p: Production(p[0],handler.getPdDescrip(p[0]),p[1]),things))
    return products


'''
Note: products is a LIST of product objects
This is command only contain one puchaseMore request
seqnum will be set after insert it into table, by default the init seqnum is 0
'''


def generatePurchaseMoreCmd(whnum, products):
    aCmd = pb2.ACommands()
    purchaseMore = aCmd.buy.add()
    purchaseMore.whnum = whnum
    for prod in products:
        # thing is Aproduct message type
        thing = purchaseMore.things.add()
        thing.id = prod.id
        thing.description = prod.description
        thing.count = prod.count
    purchaseMore.seqnum = 0
    return aCmd


'''
Note: products is a LIST od product objects
seqnum will be set after insert it into table, by default the init seqnum is 0
This is command only contain one query request
'''


def generatePackCmd(whnum, products, shipid):
    aCmd = pb2.ACommands()
    pack = aCmd.topack.add()
    pack.whnum = whnum
    for prod in products:
        thing = pack.things.add()
        thing.id = prod.id
        thing.description = prod.description
        thing.count = prod.count
    pack.shipid = shipid
    pack.seqnum = 9
    return aCmd


'''
orderId(pk in order table) = packagedId = shipId
trackingId comes from Ups side
'''


def generatePutonTruckCmd(whnum, truckid, shipid):
    aCmd = pb2.ACommands()
    putOnTruck = aCmd.load.add()
    putOnTruck.whnum = whnum
    putOnTruck.truckid = truckid
    putOnTruck.shipid = shipid
    putOnTruck.seqnum = 0
    return aCmd


def generateQueryCmd(packageid, seqnum):
    aCmd = pb2.ACommands()
    qry = aCmd.queries.add()
    qry.packageid = packageid
    qry.seqnum = seqnum
    return aCmd


'''
return the seq num from the server
@param acks: the LIST of acks
'''


def generateAckResponse(acks):
    aCmd = pb2.ACommands()
    for ack in acks:
        aCmd.acks.append(ack)
    return aCmd


def generateSimspeedCmd(simspeed):
    aCmd = pb2.ACommands()
    aCmd.simspeed = simspeed
    return aCmd


def generateDiscCmd():
    aCmd = pb2.ACommands()
    aCmd.disconnect = 1
    return aCmd


def generatePacPickupCmd(whId, shipid, dest_x,dest_y,ups_username=None):
    aCmd = upb2.AUmessage()
    pickup = aCmd.pickup
    pickup.whid = whId
    pickup.shipment_id = shipid
    if(ups_username!=None):
        pickup.ups_username = ups_username
    pickup.x = dest_x
    pickup.y = dest_y
    return aCmd

def generateSendAllLoadedCmd(dest_x, dest_y, shipid,products,truckid):
    aCmd = upb2.AUmessage()
    loadFinish = aCmd.all_loaded
    package = loadFinish.packages.add()
    package.x = dest_x
    package.y = dest_y
    package.shipment_id = shipid
    for product in products:
        thing = package.item.add()
        thing.product_id = product.id
        thing.description = product.description
        thing.count = product.count
    loadFinish.truck_id = truckid
    return aCmd

'''
this command is for extra credit, a user can rebind ups account before package arrives
'''
def generateBindUserCmd(shipId,upsAcc):
    aCmd = upb2.AUmessage()
    reBind = aCmd.bind_upsuser
    reBind.shipment_id = shipId
    reBind.ups_username = upsAcc
    return aCmd



'''
receiving this msg means productions has arrived at warehouse, waiting to pack
1. mathch the order(buy status) in time order to get orderId which is the shipId
2. change order status to "Pack"
3. send pack request(contain the shipID) to worldserver
4. send APacPickup msg to tell ups assigns truck

@param purchaseMore: the message object
'''
def handlePurchaseMoreResp(handler,purchaseMore, sockWorld, sockUps):
    # get the corresponing orderId for this response, and "Set its status to Pack"
    orderId = handler.getOrderIdOfPurchaseMoreRsp(purchaseMore)
    # !!!: this orderId is the shipId sent to wolrd for pack
    # send pack reuqest to world server
    products = generateProcutionsObjFromOrderId(orderId,handler)
    packSendWorld =generatePackCmd(whnum=purchaseMore.whnum, products=products,shipid=orderId)
    # insert this requst to request table, set its real seqNum
    reqRealStr = handler.addReqToTb(packSendWorld,2)    
    # send pack request to world server
    sendMsgStr(reqRealStr,sockWorld)
    destX,destY = handler.getDestXY(orderId)
    upsAcc = handler.getUpsAccount(orderId)
    print("sending apackpick to ups")
    # send apacpick to ups
    pickupCmd = generatePacPickupCmd(whId=purchaseMore.whnum, shipid=orderId, 
                                     dest_x=destX, dest_y=destY, ups_username=upsAcc)
    sendMsg(pickupCmd,sockUps)
    

    
'''
receiving this msg means packing finish, waiting to load
1. update status to wait
2. check if truck arrives, truck also has status in order table
3. if yes, then send load request to world server and go 4
    if no, go 2
4. change order status to load
'''
def handlePackedResp(handler,shipid,sockWorld):
    # add new status of order: wait
    # update status to wait(for truck arriving)
    
    #check if truck arrives
    # one way: 
    # 1. add one more filed call truckStatus
    # 2. listen sockUps updating truckStatus in order Table 
    # 3. keeping checking if truckid fil
    while(True):
        if(handler.getTruckStatusForOrder(shipid)):
            break
        print("checking if truck status update for the order: "+ str(shipid))
        time.sleep(5)
    truckId = handler.getTruckIdForOrder(shipid)
    whId = handler.getWhIdForOrder(shipid)
    # send load request
    loadCmd = generatePutonTruckCmd(whId,truckId,shipid)
    # insert this requst to request table, set its real seqNum
    reqRealStr = handler.addReqToTb(loadCmd,3)    
    # send load request to world server
    sendMsgStr(reqRealStr,sockWorld)
    handler.updateStatusToLoad(shipid)
    print("start to load on truck " + str(truckId) + "for order " + str(shipid) )
    pass

'''
receiving this msg means loading finish, amazon done!
1. change order status to transport
2. send ASendAllLoaded
'''
def handleLoadedResp(handler,shipId, sockUps):
    
    handler.updateStatusToTrans(shipId)
    # send loaded finish signal to ups
    destX,destY = handler.getDestXY(shipId)
    truckId = handler.getTruckIdForOrder(shipId)
    pds = generateProcutionsObjFromOrderId(shipId,handler)
    sendAllLoadedCmd = generateSendAllLoadedCmd(dest_x= destX, dest_y= destY, 
                                                shipid= shipId, products= pds, truckid= truckId)
    sendMsg(sendAllLoadedCmd,sockUps)
    print("order " + str(shipId) + " finish loading, sended signal to ups")
    pass



'''
@param msgStr: string message
for all the seq num received: call generateAckResponse() to ack
for all acks received, update its corresponding seq num we sent in our requestsTable
'''


def pasreResp(msgStr,handler, sockWorld, sockUps):
    acksToSend = [] # which is seq_num of world response 
    acksFromServer = [] #which is acks sent by server to tell it receives the seqNum we sent
    
    resp = pb2.AResponses()
    resp.ParseFromString(msgStr)
    for purchaseMore in resp.arrived:
        print("purchasemore warehouse id: " + str(purchaseMore.whnum))
        # this seq num is generated by world server, need to reply server with this ack
        print("purchasemore seq num: " + str(purchaseMore.seqnum))
        acksToSend.append(purchaseMore.seqnum)
        for thing in purchaseMore.things:
            print("product id is: " + str(thing.id))
            print("product description: " + thing.description)
            print("count is: " + str(thing.count))
        handlePurchaseMoreResp(handler,purchaseMore,sockWorld, sockUps)
        
    for packed in resp.ready:
        # need update status of order in the table
        print("packed shipid: " + str(packed.shipid))
        print("packed seqnum: " + str(packed.seqnum))
        acksToSend.append(packed.seqnum)
        # open a new thread here to handle this 
        # handlePackedResp(handler,packed.shipid)
        thread_handlePackedResp = threading.Thread(target =handlePackedResp, args =(handler,packed.shipid,sockWorld))
        thread_handlePackedResp.start()
    for l in resp.loaded:
        print("loaded shipid: " + str(l.shipid))
        print("loaded seqnum: " + str(l.seqnum))
        acksToSend.append(l.seqnum)
        handleLoadedResp(handler,l.shipid,sockUps)
    if (resp.HasField('finished')):
        print("DISCONNEC SUCCESSFULLY")
        sys.exit("disconnection")
    for e in resp.error:
        print("Error err: " + e.err)
        print("Err originseqnum: " + str(e.originseqnum))
        print("Err seqnum: " + str(e.seqnum))
        acksToSend.append(e.seqnum)
        # handleErrResp() TODO: no need right now
    for ack in resp.acks:
        print("ack num is: " + str(ack))
        acksFromServer.append(ack)
    for pg in resp.packagestatus:
        print("packagestatus packageid: " + str(pg.packageid))
        print("packagestatus status: " + pg.status)
        print("packagestatus seqnum: " + pg.seqnum)
        acksToSend.append(pg.seqnum)
        # handlePackageResp() TODO: no need right now
    print("________________END of Resp from World_____________")
        
    return acksToSend,acksFromServer
    #we have extract send back outside this functin
    # # send back all the acks
    # cmd = generateAckResponse(acksToSend)
    # sendMsg(cmd,sockWorld)
    

'''
shipId = orderId = pakcageId = pk

since the request from ups end, each order shxould only come from one warehouse(i.e. all things
in this order are avaiable in one warehouse), using the ideal that there is no stock in the warehouse,
we purchase the same amount of things to one warehouse exactly as the things in order

warehoue load balance: shipId mod the number of warehouse
'''
def handleOrder(orderId, handler, whtotalNum, sockWorld):
    # get all info(things to buy) of the shipId(orderId)
    products = generateProcutionsObjFromOrderId(orderId,handler)
    print("!!!!!!!!!!!!!" + str(orderId))
    print("check if the produdcts created " + str(products[0].id) + products[0].description) # test only
    # send request to world server to purchaseMore to one warehouse wiht id = whID
    whId = (orderId % whtotalNum) + 1
    # update the warehouseId column in OrderItem table
    handler.updateWhId(orderId,whId)
    #generate list of productions object from things
    purchaseMore = generatePurchaseMoreCmd(whId,products)
    # insert this requst to request table, set its real seqNum
    reqRealStr = handler.addReqToTb(purchaseMore,1)    
    # send purchaseMore request to world server
    sendMsgStr(reqRealStr,sockWorld)
    
    print( "send purchaseMore to worldserver")
    
    
    


def listenFrontSock(sockFront,sockWorld,handler,whTotalNum,sockUps=None):
    while(True):
        print('listen frontend.......')
        sock,address= sockFront.accept()  #waiting to new connection, being blocked
        t = threading.Thread(target=recvFrontSock, args=(sock,sockWorld,handler,whTotalNum,sockUps))
        t.start()

'''
Note: each sock connection only send one signal, then it disconncet
this funciton should be called by multiple threading, which receive signal from front end, and call handleOrder function
msg format: 1,orderId, means new order
            2,orderId, means validate new upsAcc

'''

def recvFrontSock(sock,sockWorld,handler,whTotalNum,sockUps):   
    #since only shipid will be sent through
    msg = sock.recv(128).decode()
    chunks = msg.split(',')
    orderId = int(chunks[1])
    print(orderId)
    if (int(chunks[0]) == 1):
        #hanlde the order 
        handleOrder(orderId,handler,whTotalNum,sockWorld)
        pass
    elif (int(chunks[0]) == 2):
        # re-validate the ups account
        # generate reBind msg
        upsAcc = handler.getUpsAccount(orderId)
        rebindCmd = generateBindUserCmd(orderId,upsAcc)
        print("going to send rebind request to ups ")
        sendMsg(rebindCmd,sockUps)
        
    print("handled front request, close the socket")
    sock.close()    
    
    
    
'''
@para acksToSend: a list of acks(int)
'''
def sendAcksBack(acksToSend,sockWorld):
    # send back all the acks
    cmd = generateAckResponse(acksToSend)
    sendMsg(cmd,sockWorld)


'''
this function can run on serveral threads 
'''
def listenWorldSock(sockWorld,handler,sockUps=None):
    while(True):
        print ('listen world server')
        msgStr = recMsg(sockWorld)
        print("receive response from server")
        #Here: can use multiple threads to run parseResp+ackReqSent+sendAcksBack
        acksToSend,acksFromServer = pasreResp(msgStr,handler, sockWorld, sockUps)
        handler.ackReqSent(acksFromServer) #acknowledge the request we sent
        sendAcksBack(acksToSend,sockWorld) #tell server we recev its response
    
'''
if not receiving ack for one reqeust more than longestWaitTime, resend the reqeust
to world
'''
def handleMissReq(sockWorld,longestWaitTime,checkingPeriod,handler):
    while(True):
        print("serach any unacked requests")
        msgStrs = handler.returnMissReq(longestWaitTime)
        print("amount of unack is " + str(len(msgStrs)))
        for msgStr in msgStrs:
            sendMsgStr(msgStr,sockWorld)
            print("resend one request")
        time.sleep(checkingPeriod)
    

def connectToUps(upsIp,port):
    ip_port_ups = (upsIp,port)
    sockUps = socket.socket()
    sockUps.connect(ip_port_ups)
    return sockUps

'''
parse the msg from ups:

message UAmessage{
    optional USendWorldId world_id = 1 ;
    optional UPacPickupRes pickup_res = 2;
    optional UsendArrive send_arrive = 3;
    optional UPacDelivered pac_delivered = 4;
    optional UBindRes bind_res = 5;
    optional UResendPackage resend_package = 6;
}

'''
def handleUpsReq(msgStr,handler,resendQ=None):
    req = upb2.UAmessage()
    req.ParseFromString(msgStr)
    if (req.HasField('world_id')):
        raise ValueError("The world has been connceted at begining")
  
    if (req.HasField('pickup_res')):
        pickupRes = req.pickup_res
        orderId = pickupRes.shipment_id
        truckId = pickupRes.truck_id
        trackingId = pickupRes.tracking_id
        print("receive pickup feedback from ups for order " + str(orderId))
        # update truck id for that orderId
        handler.updateTruckId(orderId,truckId)
        # update trucking_id for that orderId 
        handler.updateTrackingId(orderId,trackingId)
        # update validation of the upsAccput if sending
        if(pickupRes.HasField('is_binded')):
            if(pickupRes.is_binded):
                handler.validUpsAcc(orderId)
            pass
        pass
    if (req.HasField('send_arrive')):
        truckArrive = req.send_arrive
        truckId = truckArrive.truck_id
        wh_x = truckArrive.x
        wh_y = truckArrive.y
        # !!!update truck status of "ALL" orders delivered by this truck in this warehouse 
        # handler.updateTruckStatus(2, 5, 7)
        print("truck id type ")
        print(type(truckId))
        print("wh_x type ")
        print(type(wh_x))
        print(type(wh_y))
        # handler.updateTruckStatus(truckId, wh_x, wh_y)
        truckIdRe, wh_xRe, wh_yRe, res, idSet = handler.updateTruckStatus(truckId, wh_x, wh_y)
        print("_________________Debug___________")
        print("truckid returned is ")
        print(truckIdRe)
        print(wh_xRe)
        print(wh_yRe)
        print("the res from table")
        print(res)
        print("the idSet is: ")
        print(idSet)
        print("truck " + str(truckId) + " arrives at " + "(" + str(wh_x) + "," + str(wh_y) + ")")
        pass
    
    if (req.HasField('pac_delivered')):
        # update order status
        orderId = req.pac_delivered.shipment_id
        handler.updateStatusToDelivered(orderId)
        # send email 
        
        pass
    if (req.HasField('bind_res')):
        # update validation to true
        orderId = req.bind_res.shipment_id
        res = req.bind_res.is_binded
        if (res):
            handler.validUpsAcc(orderId)
        pass
    
    if (req.HasField('resend_package')):
        orderId = req.resend_package.shipment_id
        # resend that ship_id, put this in queue
        resendQ.put(orderId)
        print("put order to resend in task queue")
        pass
    pass


def handleResend(handler,resendQ,whtotalNum,sockWorld):
    while True:
        orderId = resendQ.get(block=True, timeout=None) # will block if necessary until a free slot is available
        # change order id to buy status
        
        # handler.resetOrderStatus(orderId);
        # add a new tuple into order table using new orderId
        # update corresponding oreder items in the orederItem table
        newId = handler.reOrder(orderId)
        handleOrder(newId, handler, whtotalNum, sockWorld)
        print( "resend order +" + str(orderId) + "new order" + str(newId)) 




def serverRun(whInit,wdId,sockUps,resendQ=None):
    try:
        handler = dbHandler.Handler()
        # open a sock to listen frontend signal
        ip_port_front = ('0.0.0.0', 9999)
        sockFront = socket.socket()
        sockFront.bind(ip_port_front)
        sockFront.listen(5)
       
        # open another sock to listen and send msg to world server
        # set up initial warehouse in database
        handler.setUpWh(whInit);
        whTotalNum = len(whInit)
        sockWorld = connectToWorld(worldId=wdId , warehouses=whInit) 
     
        #sending to world and ups once recv signal
        thread_listenFront = threading.Thread(target=listenFrontSock, args=(sockFront,sockWorld,handler,whTotalNum,sockUps))
        thread_listenFront.start()
        #recev response from world and send signal to ups
        thread_listenWorld = threading.Thread(target =listenWorldSock, args =(sockWorld,handler,sockUps))
        thread_listenWorld.start()
        
        # use another thread to hadnle missing req 
        longestWaitTime = 15 #sec send again if more than this time
        checkingPeriod = 15 # check miss request every 30s 
        thread_handleMissReq = threading.Thread(target = handleMissReq, args = (sockWorld,longestWaitTime, checkingPeriod,handler))
        thread_handleMissReq.start()
        
        
        thread_handleResend = threading.Thread(target = handleResend, args = (handler,resendQ,whTotalNum,sockWorld))
        thread_handleResend.start()
        thread_listenFront.join()
        thread_listenWorld.join()
        thread_handleMissReq.join()
        thread_handleResend.join()
        sockFront.close()
        sockWorld.close()      
    except ValueError as err:
        print(err.args)



def selfTest():
    try:
        handler = dbHandler.Handler()
        # open a sock to listen frontend signal
        ip_port_front = ('0.0.0.0', 9999)
        # open another sock to listen and send msg to world server
        whInit = [(1, 2000, 300), (2, 512, 7321), (3, 5011, 5011)]
        # set up initial warehouse in database
        handler.setUpWh(whInit);
        whTotalNum = len(whInit)
        # for test, worldId by default is 1
        sockWorld = connectToWorld(warehouses=whInit) 
        sockFront = socket.socket()
        sockFront.bind(ip_port_front)
        sockFront.listen(5) 
        #sending to world and ups once recv signal
        thread_listenFront = threading.Thread(target=listenFrontSock, args=(sockFront,sockWorld,handler,whTotalNum))
        thread_listenFront.start()
        #recev response from world and send signal to ups
        thread_listenWorld = threading.Thread(target =listenWorldSock, args =(sockWorld,handler,))
        thread_listenWorld.start()       
        # use another thread to hadnle missing req 
        longestWaitTime = 10 #sec send again if more than this time
        checkingPeriod = 10 # check miss request every 30s 
        thread_handleMissReq = threading.Thread(target = handleMissReq, args = (sockWorld,longestWaitTime, checkingPeriod,handler))
        thread_handleMissReq.start()
        
        thread_listenFront.join()
        thread_listenWorld.join()
        thread_handleMissReq.join()
        sockFront.close()
        sockWorld.close()
    except ValueError as err:
        print(err.args)

def listenUpsSock(sockUps,handler,):
    # this queue is for resend order task queue
    resendQ = queue.Queue()
    # the first msg is to connect to world
    msgStr = recMsg(sockUps)
    req = upb2.UAmessage()
    req.ParseFromString(msgStr)
    if (req.HasField('world_id')):
        # connect to that world
        wdId =req.world_id.world_id
        # open another sock to listen and send msg to world server
        whInit = [(1, 3000,3000), (2, 1000, 1000), (3, 2000, 200)]
        thread_runServer = threading.Thread(target=serverRun, args=(whInit,wdId,sockUps,resendQ))
        thread_runServer.start()
    else: 
        raise ValueError("The first msg shoud only be connect to wolrd")
    
    
    while(True):
        msgStr =recMsg(sockUps)
        #TODO!!!!
        t = threading.Thread(target=handleUpsReq, args=(msgStr,handler,resendQ))
        t.start()
    
# main function
# 1. have a globle variable seq num, which will increase by 1 each time sending a reqeust
# 2. save the corresponing request for that seq num in order to resend it if not receiving ack after some time
# two ways : 1) hashtable {seq:(type,msgStr,sneding time)} 2)save in a table in database
# diff: hashtable need to consider concurrent modification ourselves e.g. using lock
# two big threads with sub-level threads
# 1. listen to what comes from frontend, then generate and send request to world server
# 2. listen to waht comes from world server, then
# another thread keep checking seq number list, if not receiving a ack for that seq num, send again
if __name__ == "__main__":
    try:        
        handler = dbHandler.Handler()        
        # open a sock to connect to ups side
        # sockUps = connectToUps(upsIp="vcm-26404.vm.duke.edu",port=55555)
        sockUps = connectToUps(upsIp="vcm-25953.vm.duke.edu",port=55555)
        thread_listenUps = threading.Thread(target=listenUpsSock, args=(sockUps,handler))
        thread_listenUps.start()
        thread_listenUps.join()
        print("Disconncetion with ups")
        sockUps.close()
        # selfTest()
        
    except ValueError as err:
        print(err.args)
    
    
    


# command = pb2.ACommands()
# purchase = command.buy.add()
# purchase.whnum = 1

# product = purchase.things.add()
# product.id = 1
# product.description = "food"
# product.count = 10

# purchase.seqnum = 1

# # connect_string_message = connect.SerializeToString()
# command_string_message = command.SerializeToString()
# print(type(command_string_message.decode()))
# print(command_string_message)
# # while True:
# print("client send the message")     #
# # inp = input("please enter the send message: ").strip()
# # if not inp:
# #     continue
# # inp = open('testxml').read()
# _EncodeVarint(s.send, len(connect_string_message), None)
# s.sendall(connect_string_message)

# # if inp == "exit":   #
# #     print("close connection!")
# #     break
# # server_reply = s.recv(1024).decode()
# # print(type(server_reply))
# # print(server_reply)
# var_int_buff = []
# while True:
#     buf = s.recv(1)
#     var_int_buff += buf
#     msg_len, new_pos = _DecodeVarint32(var_int_buff, 0)
#     if new_pos != 0:
#         break
# whole_message = s.recv(msg_len)
# # acconected =pb2.AConnected()
# # acconected.ParseFromString(whole_message)
# # print(acconected.worldid)
# # print(acconected.result)
# resp = pb2.AResponses()
# resp.ParseFromString(whole_message)
# print(resp.acks)


# # break
# s.close()
