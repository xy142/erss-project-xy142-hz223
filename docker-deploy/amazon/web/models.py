from django.db import models
from requests import request
from django.utils.timezone import now

# Create your models here.
class User(models.Model):
    name = models.CharField(max_length=128)
    password = models.CharField(max_length=256)

class Warehouse(models.Model):
    x = models.IntegerField(default=0)
    y = models.IntegerField(default=0)

class Production(models.Model):
    FOOD = 'F'
    ELECTRONICS = 'E'
    CATALOG = [(FOOD, 'Food'), (ELECTRONICS, 'Electronics')]

    name = models.CharField(max_length=128)
    description = models.CharField(max_length=256)
    catalog = models.CharField(max_length=1, choices=CATALOG, default=FOOD)

class Order(models.Model):
    BUY = 'B' # buy but not packed
    PACK = 'P' # packing but not loaded:
    # WAIT = 'W' # two cases: 1. warehouse pack finish but truck not arriving
    #         # 2. truck arrives but warhouse not finishing packing
    LOAD = 'L' # loading but not Transported
    TRANSPORT = 'T' # trans.....
    DELIVERED = 'D'
    STATUS = [(BUY, 'Buy'), (PACK, 'Pack'), (LOAD, 'Load'), (TRANSPORT, 'Transport'),(DELIVERED,'Delivered')]
    
    
    
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    destX = models.IntegerField(default=0)
    destY = models.IntegerField(default=0)
    status = models.CharField(max_length=1, choices=STATUS, default=BUY)
    upsAcc = models.CharField(max_length=128, default='')
    upsAcc_valid = models.BooleanField(default=False)
    tracking_number = models.CharField(max_length=128, default='')
    truck_id = models.IntegerField(default=0)
    truck_status = models.BooleanField(default=False)
    
class OrderItem(models.Model):
    production = models.ForeignKey(Production, on_delete=models.CASCADE)
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    warehouse = models.ForeignKey(Warehouse, on_delete=models.CASCADE)
    amount = models.IntegerField(default=0)
    
class Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    production = models.ForeignKey(Production, on_delete=models.CASCADE)
    amount = models.IntegerField(default=0)
