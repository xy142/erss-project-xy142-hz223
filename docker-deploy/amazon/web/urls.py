from django.urls import path

from . import views

app_name = 'web'

urlpatterns = [
    path('', views.login, name='login'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
    path('register/', views.register, name='register'),
    path('index/', views.index, name='index'),
    path('public/', views.public, name='public'),
    path('checkout/', views.checkout, name='checkout'),
    path('cart/', views.cart, name='cart'),
    path('delete-cart/<slug:slug>', views.delete_cart, name='delete_cart'),
    path('production/<slug:slug>', views.production, name='production'),
    path('myorder/', views.myorder, name='myorder'),
    path('order-detail/<slug:slug>', views.order_detail, name='order_detail'),
    path('rebind-ups/<slug:slug>', views.rebind_ups, name='rebind_ups'),
]