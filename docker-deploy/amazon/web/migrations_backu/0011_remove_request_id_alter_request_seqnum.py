# Generated by Django 4.0.1 on 2022-04-17 18:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0010_request'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='request',
            name='id',
        ),
        migrations.AlterField(
            model_name='request',
            name='seqnum',
            field=models.IntegerField(primary_key=True, serialize=False),
        ),
    ]
