# Generated by Django 4.0.1 on 2022-04-16 23:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0007_wareproduction'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderitem',
            name='tracking_number',
            field=models.CharField(default='', max_length=128),
        ),
    ]
