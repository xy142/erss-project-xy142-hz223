# Generated by Django 4.0.4 on 2022-04-21 22:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0016_delete_wareproduction'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='upsAcc_valid',
            field=models.BooleanField(default=False),
        ),
    ]
