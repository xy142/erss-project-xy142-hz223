from django import forms

from .models import User
from bootstrap_datepicker_plus.widgets import DatePickerInput
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def validate_positive(value):
    if value <= 0:
        raise ValidationError(
            _('%(value)s is not a positive number'),
            params={'value': value},
        )


class RegisterForm(forms.Form):
    name = forms.CharField(label='Email', required=True, widget=forms.TextInput(
        attrs={'class': 'form-control'}))
    password = forms.CharField(label='Password', required=True,
                               widget=forms.PasswordInput(attrs={'class': 'form-control'}))


class LoginForm(forms.Form):
    name = forms.CharField(label='Email', required=True, widget=forms.TextInput(
        attrs={'class': 'form-control form-control-lg', 'placeholder': 'Enter your email'}))
    password = forms.CharField(label='Password', required=True, widget=forms.PasswordInput(
        attrs={'class': 'form-control form-control-lg', 'placeholder': 'Enter password'}))


class ProductionForm(forms.Form):
    name = forms.CharField(label='Name', required=True, widget=forms.TextInput(
        attrs={'readonly': 'readonly', 'class': 'form-control'}))
    description = forms.CharField(label='Description', required=True, widget=forms.TextInput(
        attrs={'readonly': 'readonly', 'class': 'form-control'}))
    amount_to_buy = forms.IntegerField(validators=[validate_positive], label='Amount to buy', required=True, widget=forms.NumberInput(
        attrs={'class': 'form-control', 'type': 'number', 'min': '1'}))


class CheckoutForm(forms.Form):
    x = forms.CharField(label='x coordinate of address', required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'type': 'number', 'min': '1'}))
    y = forms.CharField(label='y coordinate of address', required=True, widget=forms.TextInput(
        attrs={'class': 'form-control', 'type': 'number', 'min': '1'}))
    upsAcc = forms.CharField(label='Ups Account(optional)', required=False, widget=forms.TextInput(
        attrs={'class': 'form-control'}))
    

class RebindForm(forms.Form):
    upsAcc = forms.CharField(label='Ups Account(optional)', required=True, widget=forms.TextInput(
        attrs={'class': 'form-control'}))