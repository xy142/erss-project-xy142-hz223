from math import prod
from django.shortcuts import get_object_or_404, redirect, render
from django.db.models import Count, Sum
from django.template.defaulttags import register
# Create your views here.
from django.http import HttpResponse
from django.template import Context
import socket

from .models import Cart, Order, OrderItem, User, Production
from . import forms


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


@register.filter
def get_obj_attr(dictionary, key):
    return dictionary.get(key)


def index(request):
    production_list = Production.objects.all()
    # production2amount = getProductionAmount()
    return render(request, 'web/index.html', locals())


def cart(request):
    user = get_object_or_404(User, name=request.session['name'])
    cart_production_list = Cart.objects.filter(user=user)
    if len(cart_production_list) == 0:
        error_message = "The cart is empty!"
        return render(request, 'web/cart.html', locals())
    productionNameMap, productionDescriptionMap, productionCatalogMap = id2PorductionInfo()
    return render(request, 'web/cart.html', locals())


def delete_cart(request, slug):
    user = get_object_or_404(User, name=request.session['name'])
    cart_production_list = Cart.objects.filter(user=user)
    productionNameMap, productionDescriptionMap, productionCatalogMap = id2PorductionInfo()
    cart_obj = Cart.objects.get(id=slug)
    cart_obj.delete()
    return redirect('/cart')


def register(request):
    # if 'email' in request.session:
    #     return redirect('/index')
    if request.method == 'POST':
        register_form = forms.RegisterForm(request.POST)
        if register_form.is_valid():
            password = register_form.cleaned_data['password']
            name = register_form.cleaned_data['name']
            try:
                existed = User.objects.get(name=name)
            except:
                existed = None
            if existed is not None:
                error_message = 'name existed!'
                return render(request, 'web/register.html', locals())
            current_user = User()
            current_user.password = password
            current_user.name = name
            current_user.save()
            return redirect('/login')
    else:
        register_form = forms.RegisterForm()
    return render(request, 'web/register.html', locals())


def public(request):
    return render(request, 'web/public.html', locals())


def logout(request):
    try:
        del request.session['name']
    except KeyError:
        pass
    return redirect('/login')


def login(request):
    if request.method == 'POST':
        login_form = forms.LoginForm(request.POST)
        if login_form.is_valid():
            name = login_form.cleaned_data['name']
            password = login_form.cleaned_data['password']
            user = User.objects.filter(name=name).first()
            if not user:
                error_message = 'user does not exist'
                return render(request, 'web/login.html', locals())
            else:
                if user.password == password:
                    request.session['name'] = user.name
                    request.session.set_expiry(0)
                    return redirect('/index')
                else:
                    error_message = 'wrong name or password'
                    return render(request, 'web/login.html', locals())
    else:
        login_form = forms.LoginForm()
    return render(request, 'web/login.html', locals())


def checkout(request):
    user = get_object_or_404(User, name=request.session['name'])
    if request.method == 'POST':
        checkout_form = forms.CheckoutForm(request.POST)
        if checkout_form.is_valid():
            cart_list = Cart.objects.filter(user = user)
            x = checkout_form.cleaned_data['x']
            y = checkout_form.cleaned_data['y']
            upsAcc = checkout_form.cleaned_data['upsAcc']
            order = Order()
            order.destX = x
            order.destY = y
            order.upsAcc = upsAcc if upsAcc != None else ''
            order.user = user
            order.save()
            for cart in cart_list:
                orderItem = OrderItem()
                orderItem.amount = cart.amount
                orderItem.order = order
                orderItem.production = cart.production
                orderItem.warehouse = None
                orderItem.save()
            cart_list.delete()
            sendOrderToServer(order.id)
        return render(request, 'web/myorder.html', locals())

    checkout_form = forms.CheckoutForm()
    return render(request, 'web/checkout.html', locals())


def production(request, slug):
    production = get_object_or_404(Production, id=slug)
    user = get_object_or_404(User, name=request.session['name'])
    # production2amount = getProductionAmount()
    if request.method == 'POST':
        production_form = forms.ProductionForm(request.POST)
        if production_form.is_valid():
            name = production_form.cleaned_data['name']
            description = production_form.cleaned_data['description']
            amount_to_buy = production_form.cleaned_data['amount_to_buy']
            try:
                cart = get_object_or_404(
                    Cart, user=user, production=production)
                cart.amount += amount_to_buy
                cart.save()
            except:
                cart = Cart()
                cart.amount = amount_to_buy
                cart.user = user
                cart.production = production
                cart.save()
            production_form = forms.ProductionForm(
                initial={'name': production.name, 'description': production.description, 'amount_to_buy': cart.amount})
            error_message = 'Add to cart successfully!'
            return render(request, 'web/production.html', locals())
    try:
        cart = get_object_or_404(
            Cart, user=user, production=production)
        amount_to_buy = cart.amount
    except:
        amount_to_buy = 1
    production_form = forms.ProductionForm(
        initial={'name': production.name, 'description': production.description, 'amount_to_buy': amount_to_buy})
    return render(request, 'web/production.html', locals())


def myorder(request):
    user = get_object_or_404(User, name=request.session['name'])
    orders = Order.objects.filter(user = user)
    return render(request, 'web/myorder.html', locals())


def order_detail(request, slug):
    orderItems = OrderItem.objects.filter(order_id=slug)
    productionNameMap, productionDescriptionMap, productionCatalogMap = id2PorductionInfo()
    return render(request, 'web/order_detail.html', locals())


def rebind_ups(request, slug):
    order = get_object_or_404(Order, id = slug)
    if request.method == 'POST':
        rebind_form = forms.RebindForm(request.POST)
        if rebind_form.is_valid():
            upsAcc = rebind_form.cleaned_data['upsAcc']
            order.upsAcc = upsAcc
            order.save()
            sendUpsAccToServer(order.id)
            return redirect('/myorder')
    
    rebind_form = forms.RebindForm(initial={'upsAcc': order.upsAcc})
    return render(request, 'web/rebindUpsAcc.html', locals())


def id2PorductionInfo():
    resName = {}
    resDescription = {}
    resCatalog = {}
    production_list = Production.objects.all()
    for production in production_list:
        resName[production.id] = production.name
        resDescription[production.id] = production.description
        resCatalog[production.id] = production.catalog
    return resName, resDescription, resCatalog


def sendOrderToServer(orderId):
    ip_port = ("server", 9999)
    s = socket.socket()
    s.connect(ip_port)
    s.sendall(("1," + str(orderId)).encode())
    # s.sendall(str(orderId).encode())
    s.close()

def sendUpsAccToServer(orderId):
    ip_port = ("server", 9999)
    s = socket.socket()
    s.connect(ip_port)
    s.sendall(("2," + str(orderId)).encode())
    s.close()
